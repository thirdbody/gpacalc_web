# gpacalc_web

The ever-evolving NBPS GPA calculator, now in website form.

## Why?

The [GUI version](https://gitlab.com/Myl0g/gpacalc_gui) only works on macOS.

Normally, this wouldn't be an issue, since roughly 90% of the school runs on macOS, but *they don't all run the same version*.

My GUI app leveraged APIs from 10.13 onward, which half of the students couldn't run.

Developing a web version ensures my app can be used on any internet-connected device. Windows, Linux, macOS, iOS, Android, and whatever else exists out there!
