export default class Grade {
  public courseName: string;
  public letterReadable: string;
  public symbolReadable: string;
  public typeReadable: string;

  public letter: number;
  public symbol: number;
  public type: number;

  constructor(name: string, letter: string, symbol: string, type: string) {
    this.courseName = name;

    /// All of the decimal values used below are from the official North Broward GPA website.
    this.letterReadable = letter;
    switch (letter) {
      case "A":
        this.letter = 4.0;
        break;
      case "B":
        this.letter = 3.0;
        break;
      case "C":
        this.letter = 2.0;
        break;
      case "D":
        this.letter = 1.0;
        break;
      default:
        this.letter = 0.0;
    }

    this.symbolReadable = symbol;
    switch (symbol) {
      case "+":
        this.symbol = 0.33;
        break;
      case "-":
        this.symbol = -0.33;
        break;
      default:
        this.symbol = 0.0;
        this.symbolReadable = "";
    }

    this.typeReadable = type;
    switch (type) {
      case "Regular":
        this.type = 1.0;
        break;
      case "Honors":
        this.type = 1.15;
        break;
      case "Accelerated":
        this.type = 1.3;
        break;
      default:
        this.type = 0.0;
        this.typeReadable = "";
    }
  }

  public toString(): string {
    return `${this.courseName}: ${this.letterReadable}${this.symbolReadable} (${
      this.typeReadable
    })`;
  }
}
