import Gradebook from "./classes/Gradebook";
import {
  createNewGradeSlot,
  htmlToGrades,
  loadGradebook,
} from "./helperFunctions";

window.onload = () => {
  if (document.cookie.includes("gradebook")) {
    const gb = loadGradebook();
    for (const grade of gb.grades) {
      createNewGradeSlot(
        document,
        grade.courseName,
        grade.letterReadable,
        grade.symbolReadable,
        grade.typeReadable,
      );
    }
  }

  (document.getElementById(
    "add-more-slots",
  ) as HTMLButtonElement).onclick = () => {
    createNewGradeSlot(document);
  };

  (document.getElementById("gpa") as HTMLButtonElement).onclick = () => {
    const gradebook = new Gradebook();
    gradebook.grades = htmlToGrades(
      document.getElementById("main-div").children,
    );
    console.log(gradebook.grades);
    gradebook.save();
    alert(gradebook.gpa.toPrecision(3));
  };

  (document.getElementById("unweighted") as HTMLButtonElement).onclick = () => {
    const gradebook = new Gradebook();
    gradebook.grades = htmlToGrades(
      document.getElementById("main-div").children,
    );
    console.log(gradebook.grades);
    gradebook.save();
    alert(gradebook.unweighted.toPrecision(3));
  };
};
